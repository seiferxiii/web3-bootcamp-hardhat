const { ethers } = require('hardhat');
const hre = require('hardhat')
require("dotenv").config();

async function main() {
    const helloWorldAddress = '0x631aFfbE551DEFBDE339B0e2D1353BcA2dd5eBE4'
    const helloWorldAbi = [{ "inputs": [{ "internalType": "string", "name": "initialName", "type": "string" }], "stateMutability": "nonpayable", "type": "constructor" }, { "inputs": [], "name": "getName", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "string", "name": "newName", "type": "string" }], "name": "setName", "outputs": [], "stateMutability": "nonpayable", "type": "function" }]
    const [deployer] = await ethers.getSigners();

    const helloWorldContract = new ethers.Contract(helloWorldAddress, helloWorldAbi, deployer)

    let getName = await helloWorldContract.getName()
    console.log('getName:', getName)

    await helloWorldContract.setName("Qwerty")

    getName = await helloWorldContract.getName()
    console.log('getName:', getName)

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });