const { ethers } = require('hardhat');
const hre = require('hardhat')
require("dotenv").config();

async function main() {
    const HelloWorld = await ethers.getContractFactory("HelloWorld")
    const helloWorld = await HelloWorld.deploy("Eli")
    await helloWorld.deployed()
    console.log('helloWorld.address', helloWorld.address)
    console.log(`
        HELLO_WORLD_ADDRESS=${helloWorld.address}`)

    try {
        await hre.run('verify:verify', {
            address: helloWorld.address,
            constructorArguments: ["Eli"],
            contract: "contracts/HelloWorld.sol:HelloWorld"
        })
    } catch (e) {
        console.log(e.message)
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });