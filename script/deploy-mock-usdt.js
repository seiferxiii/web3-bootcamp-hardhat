const { ethers } = require('hardhat');
const hre = require('hardhat')
require("dotenv").config();

async function main() {
    const USDT = await ethers.getContractFactory("MockUSDT")
    const usdt = await USDT.deploy(ethers.utils.parseEther("100000000"))
    await usdt.deployed()
    console.log('usdt.address', usdt.address)
    console.log(`
        USDT_ADDRESS=${usdt.address}
    `)

    try {
        await hre.run('verify:verify', {
            address: usdt.address,
            constructorArguments: [ethers.utils.parseEther("100000000")],
            contract: "contracts/MockUSDT.sol:MockUSDT"
        })
    } catch (e) {
        console.log(e.message)
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });