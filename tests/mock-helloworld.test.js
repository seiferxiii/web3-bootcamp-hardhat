const { expect } = require("chai");
const { ethers } = require("hardhat");
describe("Hello World Test cases", () => {
    let helloWorld

    before(async () => {
        [deployer] = await ethers.getSigners();
        const HelloWorld = await ethers.getContractFactory("HelloWorld")
        helloWorld = await HelloWorld.deploy("Eli")
        await helloWorld.deployed()
        console.log('helloWorld.address', helloWorld.address)
        console.log(`
        HELLO_WORLD_ADDRESS=${helloWorld.address}
    `)
    })

    it("should fetch initial name", async () => {
        const initialName = await helloWorld.getName()
        console.log('Initial Name:', initialName)
        expect(initialName).to.eq("Eli")
    })
})
