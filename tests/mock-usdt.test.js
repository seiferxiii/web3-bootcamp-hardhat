const { expect } = require("chai");
const { ethers } = require("hardhat");
describe("Mock USDT Test cases", () => {
    let mockUsdt, deployer

    let totalSupply = "100000000"

    before(async () => {
        [deployer] = await ethers.getSigners();
        const USDT = await ethers.getContractFactory("MockUSDT")
        mockUsdt = await USDT.deploy(ethers.utils.parseEther(totalSupply))
        await mockUsdt.deployed()
        console.log('usdt.address', mockUsdt.address)
        console.log(`
        USDT_ADDRESS=${mockUsdt.address}
    `)
    })

    it("should fetch balance of deployer", async () => {
        const balanceOfUsdt = await mockUsdt.balanceOf(deployer.address)
        console.log('Balance of Deployer:', ethers.utils.formatEther(balanceOfUsdt))
        expect(balanceOfUsdt).to.eq(ethers.utils.parseEther(totalSupply))
    })
})
